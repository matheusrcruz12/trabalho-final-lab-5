create schema kanban;
use kanban;


create table usr_usuario (
  usr_id bigint unsigned not null auto_increment,
  usr_nome varchar(20) not null,
  usr_senha varchar(50) not null,
  primary key (usr_id),
  unique key uni_usuario_nome (usr_nome)
);

create table sta_status (
  sta_id bigint unsigned not null auto_increment,
  sta_desc varchar(50) not null,
  primary key (sta_id),
  unique key uni_status_nome (sta_desc)
);

create table aut_autorizacao (
  aut_id bigint unsigned not null auto_increment,
  aut_nome varchar(20) not null,
  primary key (aut_id),
  unique key uni_aut_nome (aut_nome)
);




create table uau_usuario_autorizacao (
  usr_id bigint unsigned not null,
  aut_id bigint unsigned not null,
  primary key (usr_id, aut_id),
  foreign key aut_usuario_fk (usr_id) references usr_usuario (usr_id) on delete restrict on update cascade,
  foreign key aut_autorizacao_fk (aut_id) references aut_autorizacao (aut_id) on delete restrict on update cascade
);

create table tar_tarefa (
  tar_id bigint unsigned not null auto_increment,
  tar_tarefa varchar(100) not null,
  tar_desc varchar(400) not null,
  sta_status_id bigint unsigned not null,
  usr_criacao_id bigint unsigned not null,
  usr_realizador_id bigint unsigned default null,
  tar_criacao datetime not null,
  tar_realizada datetime default null,
  primary key (tar_id),
  foreign key usr_criacao_fk (usr_criacao_id) references usr_usuario (usr_id) on delete restrict on update cascade,
  foreign key usr_realizador_fk (usr_realizador_id) references usr_usuario (usr_id) on delete restrict on update cascade,
  foreign key sta_status_fk (sta_status_id) references sta_status (sta_id) on delete restrict on update cascade
);


insert into sta_status(sta_desc) values("Espera");
insert into sta_status(sta_desc) values("Execução");
insert into usr_usuario(usr_nome, usr_senha) values('admin', concat('{md5}', md5('admin')));




insert into usr_usuario(usr_nome, usr_senha) values('teste', concat('{md5}', md5('teste')));
insert into usr_usuario(usr_nome, usr_senha) values('admin', concat('{md5}', md5('admin')));
insert into aut_autorizacao(aut_nome) values('role_usuario');
insert into aut_autorizacao(aut_nome) values('role_admin');
insert into uau_usuario_autorizacao(usr_id, aut_id)
select usr_id, aut_id
from usr_usuario, aut_autorizacao
where usr_nome = 'teste'
and aut_nome = 'role_usuario';
insert into uau_usuario_autorizacao(usr_id, aut_id)
select usr_id, aut_id
from usr_usuario, aut_autorizacao
where usr_nome = 'admin';





