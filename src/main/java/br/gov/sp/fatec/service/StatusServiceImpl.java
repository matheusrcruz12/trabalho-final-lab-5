package br.gov.sp.fatec.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.gov.sp.fatec.model.Status;
import br.gov.sp.fatec.repository.StatusRepository;

@Service("statusService")
public class StatusServiceImpl implements StatusService{

	@Autowired
	private StatusRepository statusRepo;

	@Override
	public Status salvar(Status status) {
		return statusRepo.save(status);
		
	}

	@Override
	public void excluir(Long idAStatus) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Status> todos() {
		
		return (List<Status>) statusRepo.findAll();
	}

	@Override
	public List<Status> buscarPorNome(String nome) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Status buscarPorId(Long idAStatus) {
		// TODO Auto-generated method stub
		return null;
	}
}
