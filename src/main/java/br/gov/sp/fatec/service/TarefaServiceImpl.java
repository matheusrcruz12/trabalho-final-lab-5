package br.gov.sp.fatec.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.gov.sp.fatec.model.Status;
import br.gov.sp.fatec.model.Tarefa;
import br.gov.sp.fatec.model.Usuario;
import br.gov.sp.fatec.repository.TarefaRepository;
import br.gov.sp.fatec.repository.UsuarioRepository;

@Service("tarefaService")
public class TarefaServiceImpl implements TarefaService{
	
	@Autowired
	public TarefaRepository tareRepo;

	
	@Autowired
	public UsuarioRepository usuRepo;
	
	@Override
	public Tarefa incluirTarefa(String titulo, String descricao, Status status, Usuario usuario) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Tarefa> buscarPorStatus(String nome) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Tarefa> todos() {
		
		return (List<Tarefa>) tareRepo.findAll();
	}

	@Override
	public Tarefa salvar(Tarefa tarefa) {
		Date date = new Date();
		
		Usuario usu = usuRepo
		tarefa.setTarefaIni(date);
		return tareRepo.save(tarefa);
		
	}

}
