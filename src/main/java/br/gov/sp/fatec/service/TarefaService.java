package br.gov.sp.fatec.service;

import java.util.List;

import br.gov.sp.fatec.model.Status;
import br.gov.sp.fatec.model.Tarefa;
import br.gov.sp.fatec.model.Usuario;

public interface TarefaService {
	
	public Tarefa incluirTarefa(String titulo, String descricao, Status status, Usuario usuario);

	public List<Tarefa> buscarPorStatus(String nome);
	
	public List<Tarefa> todos();
	
	public Tarefa salvar(Tarefa usuario);
	
	
	
	/**
	 * 	public Tarefa incluirTarefa(String titulo, String descricao, Status status, Usuario usuario);
	
	public List<Tarefa> buscarPorStatus(String nome);
	
	public Usuario buscar(Long id);
	
	public List<Tarefa> todos();
	
	public Usuario salvar(Usuario usuario);
	
	 */
}
