package br.gov.sp.fatec.service;

import java.util.List;

import br.gov.sp.fatec.model.Status;

public interface StatusService {
	
	public Status salvar(Status status);
	public void excluir(Long idAStatus);
	public List<Status> todos();

	public List<Status> buscarPorNome(String nome);
	public Status buscarPorId(Long idAStatus);
	
}
