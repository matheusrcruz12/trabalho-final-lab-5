package br.gov.sp.fatec.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;

import br.gov.sp.fatec.view.View;
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Table(name = "tar_tarefa")
public class Tarefa {
	
	
	@Id 
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "tar_id")
	private Long id;
	
	//TAR_TAREFA
	@Column(name = "tar_titulo")
	@JsonView({View.UsuarioResumo.class, View.UsuarioResumoAlternativo.class, View.Anotacao.class})
	private String tarefa;
	//TAR_DESCRICAO
	@Column(name = "tar_desc")
	@JsonView({View.UsuarioResumo.class, View.UsuarioResumoAlternativo.class, View.Anotacao.class})
	private String descricao;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "sta_status_id", nullable = false)
	@JsonView({View.UsuarioResumo.class, View.UsuarioResumoAlternativo.class, View.Anotacao.class})
	private Status status;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "usr_criacao_id", nullable = false)
	@JsonView({View.UsuarioResumo.class, View.UsuarioResumoAlternativo.class, View.Anotacao.class})
	private Usuario usuarioCad;
	//USR_REALIZADOR_ID
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "usr_realizador_id", nullable = false)
	@JsonView({View.UsuarioResumo.class, View.UsuarioResumoAlternativo.class, View.Anotacao.class})
	private Usuario usuarioExe;
	//TAR_INICIO
	@Column(name = "tar_criacao")
	@JsonView({View.UsuarioResumo.class, View.UsuarioResumoAlternativo.class, View.Anotacao.class})
	private Date tarefaIni;
	
	//TAR_FINALIZADO
	@Column(name = "tar_realizada")
	@JsonView({View.UsuarioResumo.class, View.UsuarioResumoAlternativo.class, View.Anotacao.class})
	private Date tarefaFin;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTarefa() {
		return tarefa;
	}

	public void setTarefa(String tarefa) {
		this.tarefa = tarefa;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Usuario getUsuarioCad() {
		return usuarioCad;
	}

	public void setUsuarioCad(Usuario usuarioCad) {
		this.usuarioCad = usuarioCad;
	}

	public Usuario getUsuarioExe() {
		return usuarioExe;
	}

	public void setUsuarioExe(Usuario usuarioExe) {
		this.usuarioExe = usuarioExe;
	}

	public Date getTarefaIni() {
		return tarefaIni;
	}

	public void setTarefaIni(Date tarefaIni) {
		this.tarefaIni = tarefaIni;
	}

	public Date getTarefaFin() {
		return tarefaFin;
	}

	public void setTarefaFin(Date tarefaFin) {
		this.tarefaFin = tarefaFin;
	}
	

}
