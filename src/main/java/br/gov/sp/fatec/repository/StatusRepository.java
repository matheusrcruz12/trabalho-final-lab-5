package br.gov.sp.fatec.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import br.gov.sp.fatec.model.Status;

public interface StatusRepository extends CrudRepository<Status, Long> {


	public Status findByStatus(String status);
	
	//public List<Status> findByNomeContainsIgnoreCase(String nome);

}
