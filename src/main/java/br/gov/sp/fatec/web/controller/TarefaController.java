package br.gov.sp.fatec.web.controller;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.annotation.JsonView;

import br.gov.sp.fatec.model.Status;
import br.gov.sp.fatec.model.Tarefa;
import br.gov.sp.fatec.service.TarefaService;
import br.gov.sp.fatec.view.View;

@RestController
@RequestMapping(value = "/tarefa")
public class TarefaController {
	
	@Autowired
	public TarefaService tareService;
	
	
	
	@RequestMapping(value = "/getAll", method = RequestMethod.GET)
	@JsonView(View.UsuarioResumoAlternativo.class)
	public ResponseEntity<List<Tarefa>> getAllTarefa() {
		return new ResponseEntity<List<Tarefa>>(tareService.todos(), HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@JsonView(View.UsuarioCompleto.class)
	public ResponseEntity<Tarefa> save(@RequestBody Tarefa tarefa, UriComponentsBuilder uriComponentsBuilder) {
		tarefa = tareService.salvar(tarefa);
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.setLocation(uriComponentsBuilder.path("/getById?id=" + tarefa.getId()).build().toUri());
		return new ResponseEntity<Tarefa>(tarefa, responseHeaders, HttpStatus.CREATED);
	}
	 

}
